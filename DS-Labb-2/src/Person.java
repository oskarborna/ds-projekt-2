
public abstract class Person {

	private String name;
	private String accessGroup;
	private int id;
	
	public Person(String name, String accessGroup, int id) {
		this.name = name;
		this.accessGroup = accessGroup;
		this.id = id;
	}
	
	
}