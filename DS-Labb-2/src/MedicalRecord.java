import java.util.ArrayList;
import java.util.List;

public class MedicalRecord {

	private Division division;
	private int id;
	private List<Person> persons;
	private String information;
	
	public MedicalRecord(Doctor doctor, Nurse nurse, Patient patient, int id, String info, String information) {
		persons = new ArrayList<>();
		persons.add(doctor);
		persons.add(nurse);
		persons.add(patient);
		this.id = id;
		division = doctor.getDivision();
		this.information = information;
	}

	
}
