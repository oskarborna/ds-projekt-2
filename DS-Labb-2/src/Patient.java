import java.util.ArrayList;
import java.util.List;

public class Patient extends Person{
	private List<MedicalRecord> records;
	
	public Patient(String name, String accessGroup, int id) {
		super(name, accessGroup, id);
		records = new ArrayList<MedicalRecord>();
		// TODO Auto-generated constructor stub
	}
	
	public void addRecord(MedicalRecord record) {
		records.add(record);
	}
}